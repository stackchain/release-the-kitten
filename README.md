## Installation
The requirements: 

In order to start the services you will need to have locally installed: 

Mysql (client only)


Docker (installed and daemon - running)

After unpacking the files in the same directory you will run:

```
// if bundle otherwise regular clone from rep
git clone -b master release-the-kitten.bundle <your-dir>
cd <your-dir>
chmod +X db.sh reset.sh run.sh
./run.sh
```
It will start a docker compose with 3 containers, the backend api, the front end and the database.

It will start the docker compose as a daemon, and it will try to open ```http://127.0.0.1:3000``` for you, if didn't happen just open it on your browser

### How to stop and clean

If you want to reset the database and stop all services just run (take care it removes the local './data')

```
./reset.sh
```

## Libraries

1. MySQL database - (ACID compliance) engine InnoDB (utf8mb4)
1. React
  * Hooks - easy to split components in logic x view
  * Material-UI - components ready to use
  * Apollo-GraphQL - graphql integration with react ready to use
1. Node.JS 
  * TypeORM - orm with components for many databases and integrated with graphql
  * TypeGraphQL - graphql + orm integrated - reflection using anotations 
  * TypeScript - typechecking
  * Koa - server http with graphql package and upload middleware
1. Docker 

## Improvements
* Reading and writting on the same db instance.
* Migrations are running inside the app (not in the scripts).
* Front end - no type check at all (few places using proptypes) - (back full typescript).
* When the user is typing in the filter there is no debounce (like redux-saga), it will be requesting immediatly (not requesting just filtering at this moment).
* The components were poorly optmized.
* The upload is one file per time (should be processed in background, non-blocking).
* Errors are not telling the users what they need to do to fix it.
* Api errors are being delivered to the user (no filter).
* No building optimization at all (running from dev).
* UI not using pub/sub, using lame waiting for the next - request to update with the new records.
* MySQL is not eficient for datalog  (never modifying a record - only appending changes)
* Request are hitting the database (should have a redis - nginx in front of the api/site)
* Not enforcing https - so between client-server can leak data
* No proxy to limit the requests
* No proxy to protect the api 
* Tab order was left behind - as well aria stuff
* No internationalization
* Upload should be built to run in background and free the user screen
* Check the file on api side - not happening - if its a virus we will receive it :D
* Virtualized window to limit the render processing
* Loading everything always - should be done using pagination
* No transitions - smooth ui/ux - brutally changing ui
* Text - expading / shrinking
* File size from client - :/ (I have to set the KoA to limit the upload)
* Loading - not blocking ui iterations - and bad designed

## Security
* Passwords inside the scripts.
* All db access using root password - no granularity of permissions.
* Robots not limited 
* No proxy to sanitize the requests
* Imports rel - missing integrity check
* Avoiding inner html  - xss react
* No regex attack protection - ReDoS (query is local :/)
* No inputs sanitization // sql injection etc (query is local :/).

## API
You can access the graphqi and check the documentation of the api - http://10.7.0.5:4000/graphql

## Other notes

### Backend
Using TypeORM and TypeGraphQL was possible to use reflections making the class consistent all over the code.

### Frontend
Due the short time available - I skipped the type checking, but I'm very happy with layout, material-ui has done a great job.
The query is not happening as you asked 
The tests were also not well implemented - just a few, but it is the main base for what to do for the others 
