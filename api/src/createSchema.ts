import { buildSchema, ResolverData } from 'type-graphql'
import { GraphQLSchema } from 'graphql'

import { DocumentResolver } from './resolvers/document.resolver'
import { Context } from './types/context'

/**
 * Build the schema - or abort the api 
 */
export default async function createSchema(): Promise<GraphQLSchema | undefined> {
  try {
    const schema = await buildSchema({
      resolvers: [DocumentResolver],
      container: ({ context }: ResolverData<Context>) => context.container,
      emitSchemaFile: true,
      validate: false,
    })
    return schema
  } catch (error) {
    console.error(`ERROR: build schema ${error}`)
    // process.abort()
  }
  return undefined
}