/**
 * 
 * Delay to race - with promise conditions
 * 
 * @param timeout time to race
 */
export default function delay(timeout: number): Promise<any> {
  return new Promise((_, reject) => {
    setTimeout(() => reject(new Error(`ERROR Operation timeout`)), timeout)
  })
}