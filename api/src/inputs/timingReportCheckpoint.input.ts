import { InputType, Field, ID } from 'type-graphql'

/**
 * For now, it only requires the timing report id
 */
@InputType()
export class TimingReportCheckpointInput {
  @Field(_type => ID)
  id!: number
}