import {MigrationInterface, QueryRunner} from "typeorm";

export class Initial1572818419353 implements MigrationInterface {

    // id - int 4 bytes unsigned looks fine TODO: check
    // uuid - char (fixed size 36) - uuid - but im here using nanoid (no panic :D)
    // createdAt - UTC
    // filename - the original filename 
    // mimetype - original file mimetype (from upload) - accoring RFC - varchar 255
    // user - user that upload the file // mocked

    // uuid_idx unique index using HASH - insta
    public async up(queryRunner: QueryRunner): Promise<any> {
      await queryRunner.query(`CREATE TABLE IF NOT EXISTS DOCUMENT_UPLOAD (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        uuid CHAR(36) NOT NULL,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        filename VARCHAR(255) NOT NULL,
        mimetype VARCHAR(255) NOT NULL,
        user CHAR(36) NOT NULL,
        size INT UNSIGNED NOT NULL,

        PRIMARY KEY (id),
        UNIQUE INDEX idx_uuid (uuid),
        UNIQUE INDEX idx_filename (filename(100))
        )
        ENGINE = InnoDB
        CHECKSUM = 1
      `)
    }

    // @ts-ignore
    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
