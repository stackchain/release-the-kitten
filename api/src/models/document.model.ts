import { Entity, Column, CreateDateColumn, Index, PrimaryGeneratedColumn } from 'typeorm'
import { ObjectType, Field, ID, Int } from 'type-graphql'

/**
 * 
 * Document upload
 * 
 * It holds the metada of the file uploaded 
 * The file contents is not stored at this moment
 */
@Entity({ name: 'DOCUMENT_UPLOAD'})
@ObjectType({ description: 'Handles media upload' })
export class DocumentUpload {
  @Field(_type => ID)
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number

  @Index('idx_uuid')
  @Field({ description: 'Internal uuid' })
  @Column({ name: 'uuid', type: 'char', unique: true, length: 36 })
    public uuid: string

  @Field({ description: 'Date of upload' })
  @CreateDateColumn({ name: 'createdAt', type: 'timestamp' })
    public createdAt: Date

  @Field({ description: 'Filename uploaded by user' })
  @Column({ name: 'filename', type: 'varchar', length: 255 })
    public filename: string

  @Field({ description: 'Mimetype of the file' })
  @Column({ name: 'mimetype', type: 'varchar', length: 255 })
    public mimetype: string
  
  @Index('idx_user')
  @Field({ description: 'User that sent the file' })
  @Column({ name: 'user', type: 'char', length: 21 })
    public user: string

  @Field(_type => Int)
  @Column({ name: 'size', type: 'int' })
    public size: number

}