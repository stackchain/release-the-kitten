import { Resolver, Arg, Mutation, Ctx, Query, Int } from 'type-graphql'
import { InjectRepository } from 'typeorm-typedi-extensions'
import { GraphQLUpload, FileUpload } from 'graphql-upload'
const nanoid = require('nanoid/async')
import { Repository } from 'typeorm'

import { DocumentUpload } from '../models/document.model'
import { Context } from '../types/context.js'

/**
 * Resolver for the - it uses typeorm-typedi to inject the models
 */
@Resolver(_of => DocumentUpload)
export class DocumentResolver {
  constructor(
    @InjectRepository(DocumentUpload) private readonly documentUploadRepository: Repository<DocumentUpload>,
  ) {}
  /**
   * 
   * Allow to query
   * 
   * @param where
   * @returns {Array<DocumentUpload>} return an array of timing reports
   */
  @Query(_returns => [DocumentUpload], { nullable: true })
  async documents() {
    const docs = await this.documentUploadRepository.find()
    return docs
  }

  /**
   * 
   * This mutation is responsible to receive the file 
   * and save it at the storage service (in this case GCS)
   * 
   * @param file The file to be processed
   * @param size The file size 
   * @returns {DocumentUpload} returns the required fields
   */
  @Mutation(_returns => DocumentUpload)
  async documentUpload(
    @Arg('file', _type => GraphQLUpload) file: FileUpload,
    @Arg('size', _type => Int) size: number,
    @Ctx() { user }: Context
  ): Promise<DocumentUpload | any> {
    const { filename, mimetype } = await file
    const uuid = await nanoid()

    // if reached here no timeout has occurred
    return await this.documentUploadRepository.save({
      filename,
      uuid,
      mimetype,
      user,
      size
    })
  }
  /**
   * 
   * This mutation is responsible to receive the file 
   * and save it at the storage service (in this case GCS)
   * 
   * @param file The file to be processed
   * @param size The file size 
   * @returns {DocumentUpload} returns the required fields
   */

  @Mutation(_returns => Boolean)
  async documentDelete(
    @Arg('filename', _type => String) filename: string,
  ): Promise<DocumentUpload | any> {

    // if reached here no timeout has occurred
    const { affected } = await this.documentUploadRepository.delete({ filename })
    return affected ? true : false
  }

}