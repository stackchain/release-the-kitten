import 'reflect-metadata'

// import { ResolverData } from 'type-graphql'
import { ApolloServer } from 'apollo-server-koa'
import { Decimal } from 'decimal.js'
import * as TypeORM from 'typeorm'
import { Container } from 'typedi'
import * as dotenv from 'dotenv'
import Koa = require('koa')

import { DocumentUpload } from './models/document.model'
import createSchema from './createSchema'
import { Context } from './types/context'

// Package initializations
dotenv.config()
TypeORM.useContainer(Container)
Decimal.set({ precision: 15, rounding: 2})

/**
 * It's building and running the migrations inside the api code thats bad :D 
 * TODO: it should be inside scripts
 */
const dbOptions: TypeORM.ConnectionOptions = {
  port: Number(process.env.API_MYSQL_PORT),
  password: process.env.API_MYSQL_PASSWORD,
  username: process.env.API_MYSQL_USER,
  database: process.env.API_MYSQL_DB,
  host: process.env.API_MYSQL_HOST,
  logging: false,
  type: 'mysql',
  entities: [ 
    DocumentUpload,
  ],
  migrations: [ __dirname + '/migrations/*{.js,.ts}'],
  migrationsTableName: 'migrations',
}

/**
 * If something goes wrong like the db connection it will fail miserable :D 
 * TODO: fix the startup process // fail and retry
 * 
 * @returns {AppoloServer} 
 */
const server = async function () {
  // migrations
  let db = false
  do {
    try {
      const connection = await TypeORM.createConnection(dbOptions)
      await connection.runMigrations()
      db = true
    } catch(e) {
      console.error('ERROR connecting to the db, retrying', e)
      await new Promise((resolve) => setTimeout(() => {
          resolve()
      }, 5000))
    }
  } while (!db)
  // server
  const app = new Koa()
  const schema = await createSchema()
  const gserver = new ApolloServer({
    schema,
    context: (): Context => {
      const user = 'mySelf'
      const container = Container.of(user)
      const context = { user, container }
      container.set('context', context)
      return context
    },
    // formatResponse: (response: any, { context }: ResolverData<Context>) => {
    //   Container.reset(context.user)
    //   return response
    // }
  })
  gserver.applyMiddleware({ app })
  app.listen({
    port: process.env.GRAPHQL_PORT || 4000
  })

  // returns the server instance
  return gserver
}

export default server