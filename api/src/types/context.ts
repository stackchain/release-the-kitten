import { ContainerInstance } from 'typedi'

export interface Context {
  container: ContainerInstance
  user: string;
}