import { ObjectType, Field, Int } from 'type-graphql'
import { DocumentUpload } from '../models/document.model'

@ObjectType()
export class DocumentsReturn {
  @Field(_type => [DocumentUpload])
  docs: DocumentUpload[]

  @Field(_type => Int)
  totalDocs: number

  @Field(_type => Int)
  totalSize: number
}