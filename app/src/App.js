import React from 'react'
import { ApolloProvider } from '@apollo/react-hooks'
import CssBaseLine from '@material-ui/core/CssBaseline'
import { ThemeProvider } from '@material-ui/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import createTheme from './helpers/createTheme'
import UploadManager from './layouts/UploadManager'
import { DocumentProvider } from './helpers/DocumentProvider'
import client from './helpers/gqlClient'

function App() {
  const usingDark = useMediaQuery('(prefers-color-scheme: dark)')
  const theme = createTheme(usingDark)
  return (
    <ApolloProvider client={client}>
      <ThemeProvider theme={theme}>
        <CssBaseLine />
        <DocumentProvider>
          <UploadManager />
        </DocumentProvider>
      </ThemeProvider>
    </ApolloProvider>
  )
}

export default App
