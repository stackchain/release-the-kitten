import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles(theme => ({
  card: {
    minHeight: 80,
  },
  button: {
    float: 'right'
  },
  action: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end'
  },
  fullWidth: {
    width: '100%'
  },
  shrinkText: {
    textOverflow: 'ellipsis'
  }
}))

function FileCardView(props) {
  const classes = useStyles()
  const {
    filename,
    size,
    handleDelete
  } = props
  // TODO i18n
  return (
    <Card elevation={0} className={classes.card}>
      <CardContent>
        <Typography className={classes.shrinkText} noWrap variant='h5' color="textSecondary" gutterBottom>
          {filename}
        </Typography>
        <div className={classes.action}>
          <span>{size}kb</span>
          <Button 
            size='small'
            variant='contained'
            color='primary'
            aria-label={`Delete the file ${filename}`}
            onClick={handleDelete}
          >
            Delete
          </Button>
        </div>
      </CardContent>
    </Card>
  )
}

FileCardView.propTypes = {
  filename: PropTypes.string.isRequired,
  size: PropTypes.number.isRequired,
  handleDelete: PropTypes.func.isRequired
}

FileCardView.defaultProps = {
  handleDelete: (e) => console.log(e)
}

export default React.memo(FileCardView)