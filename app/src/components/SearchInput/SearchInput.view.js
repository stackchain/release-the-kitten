import React from 'react'
import PropTypes from 'prop-types'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import clsx from 'clsx'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  gutters: {
    marginBottom: theme.spacing(1)
  }
}))

function SearchInputView(props) {
  const classes = useStyles()
  const {
    gutters,
    handleChange,
    width,
    inputProps,
    ...rest
  } = props
  return (
    <OutlinedInput
      className={clsx({
        [classes.gutters]: gutters
      })}
      style={{
        minWidth: width
      }}
      {...rest}
      onChange={handleChange}
      inputProps={inputProps}
      margin='dense'
      type='search'
    />
  )
}

SearchInputView.propTypes = {
  width: PropTypes.string,
  gutters: PropTypes.bool,
  handleChange: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  autoComplete: PropTypes.string,
  inputProps: PropTypes.shape({
    'aria-label': PropTypes.string
  }).isRequired
}

SearchInputView.defaultProps = {
  width: '250px'
}

export default SearchInputView