import React from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import clsx from 'clsx'

const useStyles = makeStyles(theme => ({
  input: {
    display: 'none',
  },
  flex: {
    display: 'flex'
  },
  fullWidth: {
    width: '100%'
  },
  gutters: {
    marginBottom: theme.spacing(1)
  }
}))

function UploadButtonView(props) {
  const classes = useStyles()
  const {
    id,
    label,
    ariaLabel,
    handleChange,
    fullWidth,
    inputProps,
    gutters,
    ...rest
  } = props
  const intId = `input-${id}`
  return (
    <div className={clsx(
      classes.flex,
      {
        [classes.fullWidth]: fullWidth,
        [classes.gutters]: gutters
      })}>
      <input 
        {...inputProps}
        className={classes.input}
        onChange={handleChange}
        id={intId}
        type='file'
      />
      <label id={id} htmlFor={intId} className={clsx({
        [classes.fullWidth]: fullWidth
      })}>
        <Button
          {...rest}
          aria-label={ariaLabel}
          fullWidth
          variant='contained'
          component='span'
        >
          {label}
        </Button>
      </label>
    </div>
  )
}

UploadButtonView.propTypes = {
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  color: PropTypes.oneOf(['primary', 'secondary']),
  ariaLabel: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  fullWidth: PropTypes.bool,
  gutters: PropTypes.bool,
  inputProps: PropTypes.shape({
    multiple: PropTypes.bool,
    name: PropTypes.string.isRequired,
    accept: PropTypes.string.isRequired
  })
}

UploadButtonView.defaultProps = {
  color: 'primary'
}

export default React.memo(UploadButtonView)