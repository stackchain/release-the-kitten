import React, { useState, createContext } from 'react'
import { equal as filterIn, notEqual as filterOut } from './filters'

const initiaLState = {
  data: [],
  totalDocs: 0,
  totalSize: 0,
  isLoading: false,
  filter: ''
}

const StateContext = createContext()

const DocumentProvider = ({ children }) => {
  const [state, dispatch] = useState({...initiaLState})
  return (
    <StateContext.Provider value={[state, dispatch]}>
        {children}
    </StateContext.Provider>
  )
}

const useDocumentContext = () => {
  const [state, dispatch] = React.useContext(StateContext)

  function setFilter(query) {
    dispatch(state => {
      const filteredData = state.data.filter(filterIn(query))
      return { 
        ...state,
        filter: query,
        totalDocs: filteredData.length,
        totalSize: filteredData.reduce((a, v) => a + v.size, 0)
      }
    })
  }
  function del(query) {
    dispatch(state => {
      const newData = state.data.filter(filterOut(query))
      const filteredData = newData.filter(filterIn(state.filter))
      return {
        ...state,
        data: newData,
        totalDocs: filteredData.length,
        totalSize: filteredData.reduce((a, v) => a + v.size, 0)
      }
    })
  }
  function add(doc) {
    dispatch(state => {
      const newData = [doc, ...state.data]
      return {
        ...state,
        filter: '', // clean the filter 
        data: newData,
        totalDocs: newData.length,
        totalSize: newData.reduce((a, v) => a + v.size, 0)
      }
    })
  }
  
  return { ...state, setFilter, add, del }
}

export { DocumentProvider, StateContext, useDocumentContext }