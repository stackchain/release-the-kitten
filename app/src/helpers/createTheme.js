import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles'

// using responsive fonts so we don't have to worry abou it
const theme = usingDark => { 
  return responsiveFontSizes(createMuiTheme({
    palette: {
      type: usingDark ? 'dark' : 'light'
    }
  }))
}

export default theme