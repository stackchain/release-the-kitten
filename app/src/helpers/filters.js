const equal = filter => ({ filename }) => (filter === '' || filename.indexOf(filter) !== -1)
const notEqual = filter => ({ filename }) => (filter !== filename)

export { equal, notEqual}