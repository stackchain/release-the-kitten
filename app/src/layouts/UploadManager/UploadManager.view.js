import React from 'react'
// importing using paths for treeshaking
import Container from '@material-ui/core/Container'
import { makeStyles } from '@material-ui/core/styles'

import SectionTopMenu from './sections/TopMenu'
import SectionDataList from './sections/DataList'

const useStyles = makeStyles(theme => ({
  container: {
    marginTop: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(10),
    },
  }
}))

function UploadManagerView() {
  const classes = useStyles()
  return (
    <Container maxWidth='md' className={classes.container}>
      <SectionTopMenu />
      <SectionDataList />
    </Container>
  )
}

export default UploadManagerView