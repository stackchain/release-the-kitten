import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

import './DataPanel.view'
import './InfoPanel.view'

jest.mock('@material-ui/core/styles', () => ({ 
  makeStyles: () => () => {
    return {

    }
  },
  useTheme: () => ({})
}))
jest.mock('./InfoPanel.view', () => ({ default: () => (<div></div>), __esModule: true }))
jest.mock('./DataPanel.view', () => ({ default: () => (<div></div>), __esModule: true  }))

import DataList from './DataList.view'

describe('<TopMenu>', function() {
  it('it should render', function() {
    jest
      .spyOn(React, 'useState')
      .mockImplementation(() => {})
    jest
      .spyOn(React, 'useContext')
      .mockImplementation(() => ({ state: {} }))
    const wrapper = mount(<DataList />)
    expect(wrapper).toMatchSnapshot()
  })

})