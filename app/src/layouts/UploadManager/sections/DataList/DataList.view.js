import React from 'react'
import { makeStyles } from '@material-ui/core/styles'

import InfoPanelView from './InfoPanel.view'
import DataPanelView from './DataPanel.view'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing(5),
    },
  },
}))

function SectionDataListView() {
  const classes = useStyles()
  return (
    <div className={classes.container}>
      <InfoPanelView />
      <DataPanelView />
    </div>
    )
  }
  
export default SectionDataListView