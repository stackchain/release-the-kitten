import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import gql from 'graphql-tag'
import { useQuery, useMutation } from '@apollo/react-hooks'
import Typography from '@material-ui/core/Typography'

import FileCardView from '../../../../components/FileCard'
import { useDocumentContext } from '../../../../helpers/DocumentProvider'
import { equal as filterIn } from '../../../../helpers/filters'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'grid',
    gridTemplateColumns: '1fr',
    gridRowGap: theme.spacing(1),
    justifyItems: 'stretch',
    [theme.breakpoints.up('sm')]: {
      gridTemplateColumns: 'repeat(auto-fill, minmax(220px, 1fr))',
      gridColumnGap: theme.spacing(1)
    }
  },
}))

function DataPanelView() {
  const classes = useStyles()
  const { data, filter } = useDocumentContext()
  // TODO i18n
  const renderCard = props => <FileCardLogic key={`${props.filename}-${props.size}`} {...props} />
  const keeping = filterIn(filter)
  return (
    <div className={classes.container}>
      { data.filter(keeping).map(renderCard) }
    </div>
  )
}

const DOCUMENTS = gql`
  query documents {
    documents {
      filename,
      size
    }
}
`

function DataPanelLogic() {
  const { data, add } = useDocumentContext()
  const { loading, error, data: queryData } = useQuery(DOCUMENTS, {})

  React.useEffect(() => {
    const addData = ({ filename, size }) => add({ filename, size })
    if (!error || error) {
      if (queryData) {
        const { documents } = queryData
        if (documents) {
          documents.forEach(addData)
        }
      }
    }
  }, [error, loading, queryData])
  if (loading) return <span>Loading...</span>
  return error ? <Typography variant='h3'>You are a bad user {error.message}, the kitten is sad</Typography> : <DataPanelView data={data} />
}

const DOCUMENT_DELETE = gql`
  mutation documentDelete($filename: String!) {
    documentDelete(filename: $filename)
  }
`

function FileCardLogic(props) {
  const { del } = useDocumentContext()
  const [documentDelete] = useMutation(DOCUMENT_DELETE)
  const { filename } = props
  const handleDelete = e => {
    documentDelete({ variables: { filename }})
      .then(_ => del(filename))
      .catch(e => window.alert('Bad bad user ' + e.message))
  }
  return (
    <FileCardView {...props} handleDelete={handleDelete} />
  )
}

export default DataPanelLogic