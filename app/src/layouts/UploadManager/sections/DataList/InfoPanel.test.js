import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })
import * as Ctx from '../../../../helpers/DocumentProvider'

import InfoPanel from './InfoPanel.view'

describe('<InfoPanel>', function() {
  it('it should mock the context', function() {
    const ctxState = { totalDocs: 1, totalSize: 200 }
    jest
      .spyOn(Ctx, 'useDocumentContext')
      .mockImplementation(() => ctxState)
    const wrapper = shallow(<InfoPanel />)
    expect(wrapper).toMatchSnapshot()
    expect(wrapper.find('InfoPanelView').length).toBe(1)
    expect(wrapper.find('InfoPanelView').props()).toStrictEqual(ctxState)
  })
})