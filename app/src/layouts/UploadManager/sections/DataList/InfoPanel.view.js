import React from 'react'
import PropsType from 'prop-types'
import Typography from '@material-ui/core/Typography'
import { makeStyles } from '@material-ui/core/styles'

import { useDocumentContext } from '../../../../helpers/DocumentProvider'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'baseline',
    flexGrow: 1,
    marginBottom: theme.spacing(1),
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    }
  },
}))

function InfoPanelLogic() {
  const { totalDocs, totalSize } = useDocumentContext()
  return (
    <InfoPanelView totalDocs={totalDocs} totalSize={totalSize} />
  )
}

function InfoPanelView({ totalDocs, totalSize }) {
  const classes = useStyles()
  // TODO i18n
  return (
    <div className={classes.container}>
      <Typography variant='h4'>{totalDocs} document{totalDocs === 1 ? '' : 's'}</Typography> 
      <Typography variant='body1'>Total size: {totalSize}kb</Typography>
    </div>
  )
}

InfoPanelView.propsType = {
  totalDocs: PropsType.number.isRequired,
  totalSize: PropsType.number.isRequired
}

export default InfoPanelLogic