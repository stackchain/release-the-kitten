import React from 'react'
import { shallow, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
configure({ adapter: new Adapter() })

import TopMenu from './TopMenu.view'

describe('<TopMenu>', function() {
  it('it should render', function() {
    const wrapper = shallow(<TopMenu />)
    expect(wrapper).toMatchSnapshot()
  })
  // it('<TopMenu> <SearchInputLogic> it should render', function() {
  //   // const ctxState = { state: { totalDocs: 1, totalSize: 200 }}
  //   // jest
  //   //   .spyOn(React, 'useContext')
  //   //   .mockImplementation(() => ctxState)
  //   const wrapper = shallow(<SearchInputLogic />)
  //   expect(wrapper).toMatchSnapshot()
  //   console.log(wrapper.debug({ verbose: true }))
  // })

})