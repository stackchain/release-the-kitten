import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useTheme } from '@material-ui/core/styles'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import gql from 'graphql-tag'
import { useMutation } from '@apollo/react-hooks'

import SearchInput from '../../../../components/SearchInput'
import UploadButton from '../../../../components/UploadButton'
import { useDocumentContext } from '../../../../helpers/DocumentProvider'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column-reverse',
    alignItems: 'center',
    [theme.breakpoints.up('sm')]: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
  },
}))

function SearchInputLogic({ mobile }) {
  const { filter, setFilter }  = useDocumentContext()
  const handleChange = e => setFilter(e.target.value)
  return (
    <SearchInput
      handleChange = {handleChange}
      id = 'search'
      placeholder = 'Search documents...' // TODO i18n
      name = 'search'
      value = {filter}
      width = {mobile ? '100%' : '350px'}
      autoComplete = 'upload-manager-search'
      inputProps = {{
        'aria-label': 'Input your search for documents' // TODO i18n
      }}
      gutters
    />
  )
}

const UPLOAD_FILE = gql`
  mutation documentUpload($file: Upload!, $size: Int!) {
    documentUpload(file: $file, size: $size) {
      filename,
      size
    }
  }
`
function UploadButtonLogic({ mobile }) {
  const { add } = useDocumentContext()
  const [documentUpload] = useMutation(UPLOAD_FILE)
  const handleChange = ({ target: { validity, files: [file] } }) => {
    const size = file.size // TODO fix fixed file size limit
    if (validity.valid && (size / 1024 / 1024) <= 10) {
      documentUpload({ variables: { file, size } })
        .then(({ data }) => add(data.documentUpload))
        .catch(e => window.alert('Its not us, its you, learn how to be a user please \n' + e))
    } else {
      window.alert('Whether your file is too big or is not acceptable.')
    }
  }

  return (
    <UploadButton
      id = 'upload'
      label = 'upload' // TODO i18n
      ariaLabel = 'Select your file' // TODO i18n
      fullWidth = {mobile}
      handleChange = {handleChange}
      inputProps = {{
        name: 'upload',
        accept: 'image/jpg, image/png',
      }}
      gutters
    />
  )
}

function SectionTopMenuView() {
  const classes = useStyles()
  const theme = useTheme()
  const mobile = useMediaQuery(theme.breakpoints.down('xs'))
  return (
    <div className={classes.container}>
      <SearchInputLogic mobile={mobile} />
      <UploadButtonLogic mobile={mobile} />
    </div>
  )
}

export default SectionTopMenuView