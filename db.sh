#!/bin/bash

source ./api/.env

while ! mysql -u$API_MYSQL_USER -p$API_MYSQL_PASSWORD -P$API_MYSQL_PORT -h127.0.0.1 -e "status" &> /dev/null; do
  echo "DB - waiting for db, retrying in 5s (fixed at 127.0.0.1) ... "
  sleep 5
done

echo "DB - creating db - fixed at 127.0.0.1"
mysql -u$API_MYSQL_USER -p$API_MYSQL_PASSWORD -P$API_MYSQL_PORT -h127.0.0.1 --protocol=TCP \
-e "CREATE DATABASE IF NOT EXISTS $API_MYSQL_DB"