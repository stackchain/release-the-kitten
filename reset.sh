#!/bin/bash

echo "RESETING ..."

# stoping services
docker-compose stop

# removing dir
#rm -rf api/src/keys
rm -rf data

# removing envs
rm api/.env
rm app/.env
rm .env

echo "DONE"
