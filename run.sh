#!/bin/bash
UID=$(id -u)
GID=$(id -g)
PWD=$(pwd)
ENV_FILE=$PWD/.env
API_ENV_FILE=$PWD/api/.env
APP_ENV_FILE=$PWD/app/.env
DATA_PATH=$PWD/data
KEYS_PATH=$PWD/keys
API_KEYS_PATH=$PWD/api/src/keys   #local
#API_KEYS_PATH=/usr/src/api/src/keys
MYSQL_PASS=toor
BUCKET_PAYROLL=kitten
BUCKET_TIMEOUT=10000
API_MYSQL_USER=root
API_MYSQL_PASSWORD=$MYSQL_PASS
#API_MYSQL_HOST=127.0.0.1 #local
API_MYSQL_HOST=10.7.0.2
API_MYSQL_PORT=3306
API_MYSQL_DB=kittendb
API_WORKER_INTERVAL=10000
GCS_JSON=gcs.json
APP_API_ENDPOINT=http://127.0.0.1:4000/graphql

mkdir -p $DATA_PATH
#mkdir -p $KEYS_PATH
#mkdir -p $API_KEYS_PATH # local
#mkdir -p $PWD/api/src/keys

#echo "KEYS -> Checking for your main keys "
#if [ ! -f "$KEYS_PATH/$GCS_JSON" ]; then
#  echo "@@@@@@@@@@@@  ERROR  @@@@@@@@@@@@"
#  echo "KEYS -> ERROR you need to provide the GCS key inside keys directory"
#  exit 1
#fi

echo "DOCKER  -> Checking compose env file "
if [ -f "$ENV_FILE" ]; then
  echo "DOCKER -> File .env exists using its configuration "
else
  echo "DOCKER -> File .env was not found creating one "
  echo "UID=$UID" >> "$ENV_FILE"
  echo "GID=$GID" >> "$ENV_FILE"
  echo "DATADIR=$DATA_PATH" >> "$ENV_FILE"
  echo "MYSQL_ROOT_PASSWORD=$MYSQL_PASS" >> "$ENV_FILE"
fi
echo " "
echo "API -> Checking .env file "
if [ -f "$API_ENV_FILE" ]; then
  echo "API -> File .env exists using its configuration "
else
  echo "API -> File .env was not found creating one "
  echo "GOOGLE_APPLICATION_CREDENTIALS=$API_KEYS_PATH/$GCS_JSON" >> "$API_ENV_FILE"
  echo "BUCKET_PAYROLL=$BUCKET_PAYROLL" >> "$API_ENV_FILE"
  echo "BUCKET_TIMEOUT=$BUCKET_TIMEOUT" >> "$API_ENV_FILE"
  echo "API_MYSQL_USER=$API_MYSQL_USER" >> "$API_ENV_FILE"
  echo "API_MYSQL_PASSWORD=$API_MYSQL_PASSWORD" >> "$API_ENV_FILE"
  echo "API_MYSQL_HOST=$API_MYSQL_HOST" >> "$API_ENV_FILE"
  echo "API_MYSQL_PORT=$API_MYSQL_PORT" >> "$API_ENV_FILE"
  echo "API_MYSQL_DB=$API_MYSQL_DB" >> "$API_ENV_FILE"
  echo "API_WORKER_INTERVAL=$API_WORKER_INTERVAL" >> "$API_ENV_FILE"
fi

echo " "
echo "APP -> Checking .env file "
if [ -f "$APP_ENV_FILE" ]; then
  echo "APP -> File .env exists using its configuration "
else
  echo "APP -> File .env was not found creating one "
  echo "REACT_APP_API_ENDPOINT=$APP_API_ENDPOINT" >> "$APP_ENV_FILE"
fi


echo "KEYS -> Copying keys to processes "
#cp $KEYS_PATH/* $API_KEYS_PATH 2> /dev/null 
#cp $KEYS_PATH/* $PWD/api/src/keys/

docker-compose config

docker-compose up --build -d

source ./db.sh

docker-compose stop

docker-compose up -d

open http://127.0.0.1:3000

echo "DONE"
